# README #

Maven, Hibernate 4.3.6, Spring 4.0.6, Vaadin 7.3.0

### How do I get set up? ###

1. Create MySQL DB called **test**
2. Edit username & password in **./src/main/resources/db/mysql.properties** file.
They have listed below values:

        database.username=root
        database.password=root

3. Install maven
4. If you are using IDE like Intellij Idea you must configure Tomcat or other servlet container.
The default web link is **http://localhost:8080/**
The application is deployed at the root application context.

### How do I get run it? ###

You can start the application with Tomcat or other servlet container in IDE
or
start with maven, just type in your shell (command line) following command:

        $M2_HOME/bin/mvn jetty:run

### Some notes ###
**Table will be generated automatically** by com.alextestprojects.javarush_todo.util. **DbPopulator**
after starting the application. It's configured in db/mysql.properties (database.init=true).
All configuration for it contains in spring-db.xml file (jdbc:initialize-database).