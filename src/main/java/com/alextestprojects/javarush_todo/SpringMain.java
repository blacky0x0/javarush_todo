package com.alextestprojects.javarush_todo;

import com.alextestprojects.javarush_todo.model.Todo;
import com.alextestprojects.javarush_todo.service.TodoService;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

/**
 * Author: blacky
 * Date: 25.10.14
 */
public class SpringMain {
    public static void main(String[] args) {
        String[] contexts = {"spring/spring-app.xml", "spring/spring-db.xml"};
        ConfigurableApplicationContext bf = new ClassPathXmlApplicationContext(contexts);
        //GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        //ctx.getEnvironment().setActiveProfiles(Profiles.JPA, Profiles.HSQLDB);
        //ctx.load(contexts);
        //ctx.refresh();
        //ctx.close();

        TodoService service = bf.getBean(TodoService.class);

        for (Todo item : service.getAll())
            System.out.println("item: " + item.getName());

        System.out.println("\n========Beans=in=Spring=context========");
        for (String bean : bf.getBeanDefinitionNames())
            System.out.println(bean);
        System.out.println("=======================================\n");

        bf.close();
    }
}
