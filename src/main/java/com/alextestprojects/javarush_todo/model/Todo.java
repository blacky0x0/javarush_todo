package com.alextestprojects.javarush_todo.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.Size;

import com.alextestprojects.javarush_todo.LoggerWrapper;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

/**
 * Author: blacky
 * Date: 21.10.14
 */
@Entity
@NamedQueries({
        @NamedQuery(name = Todo.DELETE,
                    query = "DELETE FROM Todo t WHERE t.id=:id"),
        @NamedQuery(name = Todo.ALL_SORTED,
                    query = "SELECT t FROM Todo t ORDER BY t.priority, t.name"),
})
@Table( name = "TODO_LIST" )
public class Todo implements Serializable {
    private static final LoggerWrapper LOG = LoggerWrapper.get(Todo.class);

    public static final int START_SEQ = 100000;

    public static final String DELETE = "Todo.delete";
    public static final String ALL_SORTED = "Todo.getAllSorted";

    public Todo() { }

    public Todo(String name, String description, int priority, Date expiration_date, boolean status) {
        this.name = name;
        this.description = description;
        this.priority = priority;
        this.creation_date = new Date();
        this.expiration_date = expiration_date;
        this.status = status;
    }

    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    //@NotEmpty
    private Integer id;

    @Column (name = "name")//, columnDefinition = "varchar(255)")
    @NotEmpty
    @Size(min = 1, max = 200)
    // max 150 code points
    private String name;

    @Column (name = "description")
    @Size(min = 0, max = 255)
    private String description = "";

    @Column (name = "priority")
    //@NotEmpty
    @Range(min=1, max=5)
    private int priority = 5;

    @Temporal(TemporalType.TIMESTAMP)
    @Column (name = "creation_date")
    //@NotEmpty
    private Date creation_date = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    @Column (name = "expiration_date")
    //@NotEmpty
    private Date expiration_date = new Date();

    @Column (name = "status")
    //@NotEmpty
    //@Range(min=0, max=1)
    private boolean status = false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public Date getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(Date expiration_date) {
        this.expiration_date = expiration_date;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Todo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", priority=" + priority +
                ", expiration_date=" + new SimpleDateFormat("yyyy-MM-dd").format(expiration_date) +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Todo that = (Todo) o;
        if (id == null || that.id == null) {
            throw LOG.getIllegalStateException("Equals '" + this + "' and '" + that + "' with null id");
        }
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        if (id == null) {
            throw LOG.getIllegalStateException("hashCode from '" + this + "' with null id");
        }
        return id.hashCode();
    }

}
