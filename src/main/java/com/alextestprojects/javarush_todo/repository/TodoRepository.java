package com.alextestprojects.javarush_todo.repository;

import com.alextestprojects.javarush_todo.model.Todo;

import java.util.List;

/**
 * Author: blacky
 * Date: 29.10.14
 */
public interface TodoRepository {

    Todo get(Integer id);

    List<Todo> getAll();

    Todo save(Todo todo);

    Todo update(Todo todo);

    boolean delete(Integer id);
}
