package com.alextestprojects.javarush_todo.repository.jpa;

import com.alextestprojects.javarush_todo.LoggerWrapper;
import com.alextestprojects.javarush_todo.model.Todo;
import com.alextestprojects.javarush_todo.repository.TodoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;

import java.util.List;

/**
 * Author: blacky
 * Date: 21.10.14
 */
@Repository
public class JpaTodoRepository implements TodoRepository {
    private static final LoggerWrapper LOG = LoggerWrapper.get(JpaTodoRepository.class);

    @PersistenceContext
    private EntityManager em;


    @Override
    public Todo get(Integer id) {
        LOG.info("get Todo item");

        return em.find(Todo.class, id);
    }

    @Override
    public List<Todo> getAll() {
        LOG.info("get all Todo items");

        return em.createNamedQuery(Todo.ALL_SORTED, Todo.class).getResultList();
    }

    @Override
    @Transactional
    public Todo save(Todo todo) {
        LOG.info("save Todo item");

        em.persist(todo);

        return todo;
    }

    @Override
    @Transactional
    public Todo update(Todo todo) {
        LOG.info("update Todo item");

        em.merge(todo);

        return todo;
    }

    @Override
    @Transactional
    public boolean delete(Integer id) {
        LOG.info("delete Todo item");

        //  1.    User ref = em.getReference(Todo.class, id);
        //        em.remove(ref);

        //        TypedQuery<User> query =
        //              em.createQuery("DELETE FROM Todo t WHERE t.id=:id", Todo.class);
        //  2.    return query.setParameter("id", id).executeUpdate() != 0;

        return em.createNamedQuery(Todo.DELETE)
                .setParameter("id", id)
                .executeUpdate() != 0;
    }

}
