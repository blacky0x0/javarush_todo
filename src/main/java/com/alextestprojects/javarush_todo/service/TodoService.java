package com.alextestprojects.javarush_todo.service;

import com.alextestprojects.javarush_todo.model.Todo;
import com.alextestprojects.javarush_todo.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author: blacky
 * Date: 21.10.14
 */
@Service
public class TodoService {

    @Autowired
    private TodoRepository repository;

    public Todo get(Integer id) {
        return repository.get(id);
    }

    public List<Todo> getAll() {
        return repository.getAll();
    }

    public Todo save(Todo todo) {
        return repository.save(todo);
    }

    public Todo update(Todo todo) {
        return repository.update(todo);
    }

    public boolean delete(Integer id) {
        return repository.delete(id);
    }

}
