package com.alextestprojects.javarush_todo.web;

import com.alextestprojects.javarush_todo.web.constant.TodoColumn;
import com.vaadin.data.Container;
import com.vaadin.data.Item;

/**
 * Author: blacky
 * Date: 04.11.14
 */
public class TodoFilter implements Container.Filter {
    private String needle;

    public TodoFilter(String needle) {
        this.needle = needle.toLowerCase();
    }

    public boolean passesFilter(Object itemId, Item item) {
        String haystack = ("" + item.getItemProperty(TodoColumn.STATUS.toString()).getValue());
        return haystack.contains(needle);
    }

    public boolean appliesToProperty(Object id) {
        return true;
    }
}