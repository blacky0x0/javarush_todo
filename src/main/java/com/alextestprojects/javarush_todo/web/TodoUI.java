package com.alextestprojects.javarush_todo.web;


import com.alextestprojects.javarush_todo.LoggerWrapper;
import com.alextestprojects.javarush_todo.model.Todo;
import com.alextestprojects.javarush_todo.service.TodoService;
import com.alextestprojects.javarush_todo.web.constant.Action;
import com.alextestprojects.javarush_todo.web.constant.Priority;
import com.alextestprojects.javarush_todo.web.constant.Status;
import com.alextestprojects.javarush_todo.web.constant.TodoColumn;
import com.alextestprojects.javarush_todo.web.ui.TodoWindowEditor;
import com.jensjansson.pagedtable.PagedTable;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.FieldEvents;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.Component;
import com.vaadin.ui.themes.Reindeer;
import com.vaadin.ui.themes.ValoTheme;
import org.hibernate.annotations.Filters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ru.xpoft.vaadin.VaadinMessageSource;

import javax.annotation.PostConstruct;
import java.security.GeneralSecurityException;
import java.util.*;
import java.util.logging.Filter;

@org.springframework.stereotype.Component
@Scope("prototype")
@Title("Todo list management")
@Theme("mytheme")
public class TodoUI extends UI {
    private static final LoggerWrapper LOG = LoggerWrapper.get(TodoUI.class);

    @Autowired
    private TodoService service;

    @Autowired
    private VaadinMessageSource messageSource;

    @Autowired
    private TodoWindowEditor todoWindowEditor;


    /* User interface components are stored in session */

    // UI elements
    private final Label      header = new Label();
    private final Button     createBtn = new Button();
    private final Label      filterLabel = new Label();
    private final ComboBox   filterCombo = new ComboBox();
    private final PagedTable table = new PagedTable();
    private final Label      footer = new Label();

    // Create the mainLayout root layout for the UI
    private final VerticalLayout   mainLayout = new VerticalLayout();
    private final HorizontalLayout secondLayout = new HorizontalLayout();

    // Data source container for table
    private IndexedContainer tableContainer;


    @Override
    protected void init(VaadinRequest request) {
        LOG.info("init");
        LOG.info(service.getAll().toString());

        initLocale();

        // Set main layout
        setContent(mainLayout);

        addComponents();
    }

    private void initLocale() {
        // Set the locale
        //UI.getCurrent().getSession().setLocale(Locale.ENGLISH);
        //UI.getCurrent().getSession().setLocale(new Locale("RU"));
        UI.getCurrent().getSession().setLocale(getLocale());
    }

    private void addComponents() {

        // 01. Add header to the mainLayout
        addHeader();

        // 02. Add createBtn, filterLabel filterCombo at secondLayout at mainLayout
        addControls();

        // 03. Add table to mainLayout
        addTable();

        // 04. Add footer to mainLayout
        addFooter();

        // 05. Set table at TodoWindowEditor
        todoWindowEditor.setTable(table);
    }


    public void addHeader() {

        String headerText = messageSource.getMessage("site.header");
        header.setValue("<h1 style='text-align:center'>"+ headerText +"</h1>");
        header.setSizeFull();
        header.setContentMode(ContentMode.HTML);

        mainLayout.addComponent(header);
    }

    private void addControls() {
        createBtn.setCaption(messageSource.getMessage("site.button.create_todo"));
        filterLabel.setCaption(messageSource.getMessage("site.combo.filter_todo"));

        createBtn.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {

                Collection<Container.Filter> filters = tableContainer.getContainerFilters();
                tableContainer.removeAllContainerFilters();
                table.refreshRowCache();

                todoWindowEditor.updateElementsCaption(Action.CREATE);
                todoWindowEditor.setVisible(true);

                UI.getCurrent().addWindow(todoWindowEditor);

//                for (Container.Filter filter : filters)
//                    tableContainer.addContainerFilter(filter);

                table.refreshRowCache();
            }
        });

        filterCombo.addItem(Status.ALL);
        filterCombo.addItem(Status.DONE);
        filterCombo.addItem(Status.NOT_DONE);

        filterCombo.setValue(Status.ALL);
        filterCombo.setNullSelectionAllowed(false);
        filterCombo.setItemCaption(Status.ALL, messageSource.getMessage("site.combo.filter_todo.all"));
        filterCombo.setItemCaption(Status.DONE, messageSource.getMessage("site.combo.filter_todo.done"));
        filterCombo.setItemCaption(Status.NOT_DONE, messageSource.getMessage("site.combo.filter_todo.not_done"));

        filterCombo.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {

                LOG.info(event.toString());// event.getProperty().getValue() -> "Не сделанные"

                if (filterCombo.getValue() != null) {

                    tableContainer.removeAllContainerFilters();

                    if(filterCombo.getValue() != Status.ALL)
                        tableContainer.addContainerFilter(
                                new TodoFilter("" + Status.getBooleanValue((Status) filterCombo.getValue())));

                    table.refreshRowCache();
                    table.setCurrentPage(0);
                    table.nextPage();   // sth wrong with PagedTable
                    table.nextPage();   // it's quick fix
                    table.nextPage();   // TODO: realize own paging
                    table.setCurrentPage(0);
                }

            }
        });

        secondLayout.addComponent(createBtn);
        secondLayout.addComponent(filterLabel);
        secondLayout.addComponent(filterCombo);

        secondLayout.setWidth(100, Unit.PERCENTAGE);
        secondLayout.setComponentAlignment(createBtn, Alignment.MIDDLE_LEFT);
        secondLayout.setComponentAlignment(filterLabel, Alignment.MIDDLE_RIGHT);
        secondLayout.setComponentAlignment(filterCombo, Alignment.MIDDLE_RIGHT);
        secondLayout.setExpandRatio(createBtn, 0.6f);
        secondLayout.setExpandRatio(filterLabel, 0.1f);
        //secondLayout.setExpandRatio(filterCombo, 0.2f);
        secondLayout.setMargin(true);

        // Add secondLayout to mainLayout
        mainLayout.addComponent(secondLayout);
    }

    @SuppressWarnings("unchecked")
    private void initTableDatasource() {

        IndexedContainer ic = new IndexedContainer();

        for (int i = 0; i < TodoColumn.values().length; ++i)
            ic.addContainerProperty(TodoColumn.getColumnNames()[i],
                    TodoColumn.getColumnClasses()[i],
                    TodoColumn.getColumnDefaultValues()[i]);

        //for (String p : columnNames)
        //    ic.addContainerProperty(p, String.class, "");


        List<Todo> todoItems = service.getAll();

        for (Todo item : todoItems) {
            Object id = ic.addItem();
            ic.getContainerProperty(id, TodoColumn.ID.toString()).setValue(item.getId());
            ic.getContainerProperty(id, TodoColumn.NAME.toString()).setValue(item.getName());
            ic.getContainerProperty(id, TodoColumn.DESCRIPTION.toString()).setValue(item.getDescription());
            ic.getContainerProperty(id, TodoColumn.PRIORITY.toString()).setValue(item.getPriority());
            ic.getContainerProperty(id, TodoColumn.CREATION_DATE.toString()).setValue(item.getCreation_date());
            ic.getContainerProperty(id, TodoColumn.EXPIRATION_DATE.toString()).setValue(item.getExpiration_date());
            ic.getContainerProperty(id, TodoColumn.STATUS.toString()).setValue(item.getStatus());
        }

        tableContainer = ic;
    }

    /*
    Initialize & add table at the mainLayout
     */
    private void addTable() {

        initTableDatasource();

        table.setContainerDataSource(tableContainer);
        //table.setVisibleColumns(TodoColumn.getColumnNames());
        table.setVisibleColumns(TodoColumn.getVisibleColumnNames());


        table.setColumnHeader(TodoColumn.NAME.toString(),           messageSource.getMessage("site.table.name"));
        table.setColumnHeader(TodoColumn.DESCRIPTION.toString(),    messageSource.getMessage("site.table.description"));
        table.setColumnHeader(TodoColumn.PRIORITY.toString(),       messageSource.getMessage("site.table.priority"));
        table.setColumnHeader(TodoColumn.CREATION_DATE.toString(),  messageSource.getMessage("site.table.creation_date"));
        table.setColumnHeader(TodoColumn.EXPIRATION_DATE.toString(),messageSource.getMessage("site.table.expiration_date"));
        table.setColumnHeader(TodoColumn.STATUS.toString(),         messageSource.getMessage("site.table.status"));

        // http://demo.vaadin.com/book-examples-vaadin7/book/#component.table.generatedcolumns.layoutclick
        table.addGeneratedColumn(
                messageSource.getMessage("site.table.edit"),
                new Table.ColumnGenerator() {

                    @Override
                    public Component generateCell(Table source,
                                                  final Object itemId, Object columnId) {

                        final Button edit = new Button(messageSource.getMessage("site.table.edit"));
                        edit.setData(itemId); // Store item ID
                        edit.addStyleName(Reindeer.BUTTON_LINK);

                        // Forward clicks on the layout as selection in the table
                        edit.addClickListener(new Button.ClickListener() {
                            public void buttonClick(Button.ClickEvent event) {
                                table.select(itemId);

                                todoWindowEditor.updateElementsCaption(Action.UPDATE);
                                todoWindowEditor.updateElementsValue(table, itemId);

                                todoWindowEditor.setVisible(true);

                                UI.getCurrent().addWindow(todoWindowEditor);

                            }
                        });

                        return edit;
                    }
                });


        // http://demo.vaadin.com/book-examples-vaadin7/book/#component.table.generatedcolumns.layoutclick
        table.addGeneratedColumn(
                messageSource.getMessage("site.table.delete"),
                new Table.ColumnGenerator() {

                    @Override
                    public Component generateCell(Table source,
                                                  final Object itemId, Object columnId) {

                        final Button delete = new Button(messageSource.getMessage("site.table.delete"));
                        delete.setData(source
                                .getItem(itemId)
                                .getItemProperty(TodoColumn.ID.toString()).getValue()); // Store item ID
                        delete.addStyleName(ValoTheme.BUTTON_LINK);
                        delete.addStyleName(ValoTheme.LABEL_LIGHT);

                        // Forward clicks on the layout as selection in the table
                        delete.addClickListener(new Button.ClickListener() {
                            public void buttonClick(Button.ClickEvent event) {
                                // todoId != itemId
                                table.select(itemId);

                                Object todoId = delete.getData();

                                if (service.delete((Integer) todoId)) {
                                    boolean result = table.removeItem(itemId);

                                    if (result)
                                        Notification.show("Item #" + todoId + " was deleted", Notification.Type.TRAY_NOTIFICATION);
                                    else
                                        Notification.show("Item #" + todoId + " wasn't deleted", Notification.Type.WARNING_MESSAGE);

                                    LOG.info("Item #" + todoId + " was deleted: " + result);

                                } else {
                                    LOG.error("Can't delete item #" + todoId);
                                    Notification.show("Can't delete item #" + todoId, Notification.Type.ERROR_MESSAGE);
                                }
                            }
                        });

                        return delete;
                    }
                });

        // Header, Stripes, Expand ratios, Vertical lines
        // http://demo.vaadin.com/sampler/#ui/data-presentation/table


        // Set table parameters
        table.setRowHeaderMode(Table.RowHeaderMode.INDEX);
        table.setColumnReorderingAllowed(true);
        table.setSelectable(true);
        table.setMultiSelect(false);
        table.setSizeFull();
        table.setImmediate(true);
        //table.setHeight(400, Unit.PIXELS);    //se it with paging

        
        HorizontalLayout tableControls = table.createControls();
        
        // Quick crutches for ComboBox in tableControls. TODO: realize own paging
        for (int i = 0; i < tableControls.getComponentCount(); i++) {

            if (tableControls.getComponent(i) instanceof HorizontalLayout) {
                HorizontalLayout inner = (HorizontalLayout) tableControls.getComponent(i);

                for (int j = 0; j < inner.getComponentCount(); j++) {
                    if (inner.getComponent(j) instanceof ComboBox) {
                        ComboBox itemsPerPageSelect = (ComboBox) inner.getComponent(j);
                        itemsPerPageSelect.setWidth(100, Unit.PIXELS);
                        continue;
                    }

                    if (inner.getComponent(j) instanceof TextField) {
                        TextField currentPageTextField = (TextField) inner.getComponent(j);
                        currentPageTextField.setWidth(40, Unit.PIXELS);
                        currentPageTextField.removeAllValidators();
                    }
                }
            }
        }

        // default is 25
        table.setPageLength(5);     // set 5 items per page

        mainLayout.addComponent(table);
        mainLayout.setComponentAlignment(table, Alignment.MIDDLE_CENTER);
        mainLayout.addComponent(tableControls);
    }

    private void addFooter() {

        String footerText = messageSource.getMessage("site.footer.author");
        footer.setValue("<h4 style='text-align:center'>Author: " + footerText + "</h4>");
        footer.setSizeFull();
        footer.setContentMode(ContentMode.HTML);

        mainLayout.addComponent(footer);
        mainLayout.setComponentAlignment(footer, Alignment.BOTTOM_CENTER);
    }

    @PostConstruct
    public void PostConstruct() throws GeneralSecurityException
    {
        LOG.info("PostConstruct");
    }

}