package com.alextestprojects.javarush_todo.web.constant;

/**
 * Author: blacky on 04.11.14.
 */
public enum Action {
    CREATE, UPDATE
}