package com.alextestprojects.javarush_todo.web.constant;

import com.alextestprojects.javarush_todo.LoggerWrapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * Author: blacky
 * Date: 30.10.14
 */
public enum Priority {

    P1(1),
    P2(2),
    P3(3),
    P4(4),
    P5(5);

    private static final LoggerWrapper LOG = LoggerWrapper.get(Priority.class);

    private int value;
    private Priority (int value) {
        this.value = value;
    }

    public static Collection<?> getCollection() {
        return Arrays.asList(values());
    }

    public static Collection<?> getCollectionValues() {
        ArrayList<Integer> result = new ArrayList<>();

        for (Priority item : values()) {
            result.add(item.value);
        }

        return result;
    }

    public static Priority getDefaultPriority() {
        return P5;
    }

    public static int getDefaultPriorityNumber() {
        return getDefaultPriority().value;
    }

    public static int getPriorityValue(Priority p) {
        return p.value;
    }

    public static Priority getPriority(int i) {
        switch(i) {
            case 1:
                return P1;
            case 2:
                return P2;
            case 3:
                return P3;
            case 4:
                return P4;
            case 5:
                return P5;
        }

        LOG.throwIllegalArgumentException("Incorrect priority value: " + i);
        return P1;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
