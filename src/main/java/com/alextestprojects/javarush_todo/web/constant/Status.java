package com.alextestprojects.javarush_todo.web.constant;

/**
 * Author: blacky
 * Date: 04.11.14
 */
public enum Status {
    ALL(-1),
    NOT_DONE(0),
    DONE(1);

    private int value;

    public static int getValue(Status status) {

        if (status == NOT_DONE)
            return NOT_DONE.value;

        if (status == DONE)
            return DONE.value;

        return ALL.value;
    }

    public static boolean getBooleanValue(Status status) {
        return status == DONE;
    }

    private Status(int value) {
        this.value = value;
    }

}
