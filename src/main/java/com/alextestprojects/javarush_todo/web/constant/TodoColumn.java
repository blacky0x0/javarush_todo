package com.alextestprojects.javarush_todo.web.constant;

import java.util.Date;

/**
 * Author: blacky
 * Date: 04.11.14
 */
public enum TodoColumn {

    ID                  ("id",              Integer.class,  null),
    NAME                ("name",            String.class,   ""),
    DESCRIPTION         ("description",     String.class,   ""),
    PRIORITY            ("priority",        Integer.class,  5),
    CREATION_DATE       ("creation_date",   Date.class,     new Date()),
    EXPIRATION_DATE     ("expiration_date", Date.class,     new Date()),
    STATUS              ("status",          Boolean.class,  false);

    private String name;
    private Class clazz;
    private Object defaultValue;
    private final static String[]   columnNames             = new String[values().length];
    private final static String[]   visibleColumnNames
            = new String[] {NAME.name, PRIORITY.name, EXPIRATION_DATE.name, STATUS.name};
    private final static Class[]    columnClasses           = new Class[values().length];
    private final static Object[]   columnDefaultValues     = new Object[values().length];


    private TodoColumn(String name, Class clazz, Object defaultValue) {
        this.name = name;
        this.clazz = clazz;
        this.defaultValue = defaultValue;
    }

    public static String[] getVisibleColumnNames() {
        return visibleColumnNames;
    }

    public static String[] getColumnNames() {

        for (int i = 0; i < values().length; i++) {
            columnNames[i] =  values()[i].name;
        }

        return columnNames;
    }

    public static Class[] getColumnClasses() {
        for (int i = 0; i < values().length; i++) {
            columnClasses[i] =  values()[i].clazz;
        }

        return columnClasses;
    }

    public static Object[] getColumnDefaultValues() {

        for (int i = 0; i < values().length; i++) {
            columnDefaultValues[i] =  values()[i].defaultValue;
        }

        return columnDefaultValues;
    }

    @Override
    public String toString() {
        return name;
    }
}