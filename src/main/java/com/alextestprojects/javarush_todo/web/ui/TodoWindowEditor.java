package com.alextestprojects.javarush_todo.web.ui;

import com.alextestprojects.javarush_todo.LoggerWrapper;
import com.alextestprojects.javarush_todo.model.Todo;
import com.alextestprojects.javarush_todo.service.TodoService;
import com.alextestprojects.javarush_todo.web.constant.Action;
import com.alextestprojects.javarush_todo.web.constant.Priority;
import com.alextestprojects.javarush_todo.web.constant.Status;
import com.alextestprojects.javarush_todo.web.constant.TodoColumn;
import com.jensjansson.pagedtable.PagedTable;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import ru.xpoft.vaadin.VaadinMessageSource;

import java.util.Date;

/**
 * Author: blacky on 04.11.14.
 */
@org.springframework.stereotype.Component
public class TodoWindowEditor extends Window {
    private final LoggerWrapper LOG = LoggerWrapper.get(TodoWindowEditor.class);

    @Autowired
    private TodoService service;

    @Autowired
    private VaadinMessageSource messageSource;

    // Transferred values from updateElementsValue(Table, final Object) method
    private PagedTable table;
    private Object tableItemId;

    public static final int WINDOW_WIDTH = 420; // px

    private Action curAction = Action.CREATE;

    private Todo curBean = new Todo();
    // Form for editing the bean
    final BeanFieldGroup<Todo> binder = new BeanFieldGroup<>(Todo.class);

    // Container for elements
    private final Panel formPanel = new Panel("Todo");
    private final FormLayout formLayout = new FormLayout();
    private final HorizontalLayout controlsLayout = new HorizontalLayout();

    // Components
    //private final TextField idField = new TextField("id");
    private final TextField nameField = new TextField("Name");
    private final TextArea  descriptionField = new TextArea("Description");
    private final ComboBox  priorityCombo = new ComboBox("Priority");//, Priority.getCollection());
    private final DateField expirationDate = new DateField("Expiration Date", new Date());
    private final CheckBox  statusCheckBox = new CheckBox("Done ?", false);

    // Controls
    private final Button okBtn = new Button("OK");
    private final Button cancelBtn = new Button("Cancel");


    public TodoWindowEditor() {
        super();

        setVisible(false);
        center();               // Sets this window to be centered
        setModal(true);         // Sets window modality
        setResizable(false);    // change the size
        setDraggable(true);     // window can be dragged
        setWidth(WINDOW_WIDTH, Unit.PIXELS);


        // Binding bean with GUI elements
        // https://vaadin.com/book/vaadin7/-/page/datamodel.itembinding.html#datamodel.itembinding.beanvalidation
        resetBeanValues();

        // Add components to formPanel
        addWindowComponents();

        // Finish initializing window
        setContent(formPanel);

        // Set events for control buttons
        setEventHandlers();
    }

    private void addWindowComponents() {

        // NAME FIELD
        //formLayout.addComponent(binder.buildAndBind("Name", TodoColumn.NAME.toString())); // bind to bean: variant #01
        binder.bind(nameField, TodoColumn.NAME.toString()); // bind to bean: variant #02
        nameField.setWidth(100, Unit.PERCENTAGE);
        formLayout.addComponent(nameField);

        // DESCRIPTION FIELD
        binder.bind(descriptionField, TodoColumn.DESCRIPTION.toString());       // bind to bean
        descriptionField.setWidth(100, Unit.PERCENTAGE);
        formLayout.addComponent(descriptionField);

        // PRIORITY COMBOBOX
        priorityCombo.addItems(Priority.getCollectionValues());
        priorityCombo.setValue(Priority.P5.toString());
        priorityCombo.setNullSelectionAllowed(false);
        priorityCombo.setWidth(100, Unit.PERCENTAGE);
        binder.bind(priorityCombo, TodoColumn.PRIORITY.toString());
        formLayout.addComponent(priorityCombo);

        // EXPIRATION_DATE DATE_FIELD
        binder.bind(expirationDate, TodoColumn.EXPIRATION_DATE.toString());
        expirationDate.setDateFormat("yyyy-MM-dd");
        expirationDate.setWidth(100, Unit.PERCENTAGE);
        formLayout.addComponent(expirationDate);

        // STATUS CHECKBOX
        binder.bind(statusCheckBox, TodoColumn.STATUS.toString());
        statusCheckBox.setWidth(100, Unit.PERCENTAGE);
        formLayout.addComponent(statusCheckBox);


        // Buffer the form content
        binder.setBuffered(true);


        // Add controls
        controlsLayout.addComponent(okBtn);
        controlsLayout.addComponent(cancelBtn);

        // Set properties for layouts
        controlsLayout.setWidth(100, Unit.PERCENTAGE);
        controlsLayout.setComponentAlignment(okBtn, Alignment.MIDDLE_LEFT);
        controlsLayout.setComponentAlignment(cancelBtn, Alignment.MIDDLE_RIGHT);
        //controlsLayout.setMargin(true);

        formLayout.setMargin(true);

        formLayout.addComponent(controlsLayout);

        formPanel.setContent(formLayout);

    }


    private void setEventHandlers() {

        okBtn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                LOG.info("try to commit ");
                try
                {
                    binder.commit();
                }
                catch (FieldGroup.CommitException e)
                {
                    LOG.error("FieldGroup.CommitException");
                    return;
                }

                // BEAN IS COMMITTED
                if (curAction == Action.CREATE)
                {
                    curBean = service.save(curBean);

                    if (table != null)
                    {
                        tableItemId = table.addItem();
                        updateTableItem(table, tableItemId);
                        Notification.show("Item was created!");
                    }
                }

                if (curAction == Action.UPDATE)
                {
                    curBean = service.update(curBean);

                    if (table != null && tableItemId != null)
                    {
                        updateTableItem(table, tableItemId);
                        Notification.show("Item was updated!");
                    }
                }

                LOG.info(curBean.toString());

                table.refreshRowCache();
                table.select(tableItemId);
                table.setCurrentPage(table.getTotalAmountOfPages());
                table.setCurrentPageFirstItemId(tableItemId);
                tableItemId = null;

                resetBeanValues();
                close();
            }
        });

        cancelBtn.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                resetBeanValues();
                close();
            }
        });
    }

    @SuppressWarnings("unchecked")
    private void updateTableItem(Table table, Object id) {
        table.getContainerProperty(id, TodoColumn.ID.toString()).setValue(curBean.getId());
        table.getContainerProperty(id, TodoColumn.NAME.toString()).setValue(curBean.getName());
        table.getContainerProperty(id, TodoColumn.DESCRIPTION.toString()).setValue(curBean.getDescription());
        table.getContainerProperty(id, TodoColumn.PRIORITY.toString()).setValue(curBean.getPriority());
        table.getContainerProperty(id, TodoColumn.CREATION_DATE.toString()).setValue(curBean.getCreation_date());
        table.getContainerProperty(id, TodoColumn.EXPIRATION_DATE.toString()).setValue(curBean.getExpiration_date());
        table.getContainerProperty(id, TodoColumn.STATUS.toString()).setValue(curBean.getStatus());
    }

    public void setTable(PagedTable table) {
        this.table = table;
    }

    private void resetBeanValues() {
        curBean.setId(null);
        curBean.setName("");
        curBean.setDescription("");
        curBean.setStatus(false);
        curBean.setCreation_date(new Date());
        //curBean.setExpiration_date();
        curBean.setPriority((Integer) Priority.getDefaultPriorityNumber());

        // must be called immediately after reinitialize bean
        // to update values in elements
        updateBeanDataSource();
    }

    private void updateBeanDataSource() {
        binder.setItemDataSource(curBean);
    }

    /**
     * Entry point to update value
     * @param source table to update
     * @param itemId selected row in the source table
     */
    public void updateElementsValue(PagedTable source, final Object itemId) {

        setTable(source);
        tableItemId = itemId;

        Item item = source.getItem(itemId);

        curBean.setId((Integer)           item.getItemProperty(TodoColumn.ID.toString()).getValue());
        curBean.setName((String)          item.getItemProperty(TodoColumn.NAME.toString()).getValue());
        curBean.setDescription((String)   item.getItemProperty(TodoColumn.DESCRIPTION.toString()).getValue());
        curBean.setStatus((Boolean)       item.getItemProperty(TodoColumn.STATUS.toString()).getValue());
        curBean.setCreation_date((Date)   item.getItemProperty(TodoColumn.CREATION_DATE.toString()).getValue());
        curBean.setExpiration_date((Date) item.getItemProperty(TodoColumn.EXPIRATION_DATE.toString()).getValue());
        curBean.setPriority((Integer)     item.getItemProperty(TodoColumn.PRIORITY.toString()).getValue());

        // must be called immediately after reinitialize bean
        // to update values in elements
        updateBeanDataSource();
    }


    /**
     * Updating captions of elements & header of the window in keeping with Locale & action
     * @param action - Action.CREATE or Action.UPDATE
     */
    public void updateElementsCaption(Action action) {

        curAction = action;

        if (action == Action.CREATE)
            formPanel.setCaption(messageSource.getMessage("site.window.create.todo"));
        else
            formPanel.setCaption(messageSource.getMessage("site.window.update.todo"));

        nameField.setCaption(messageSource.getMessage("site.table.name"));
        descriptionField.setCaption(messageSource.getMessage("site.table.description"));
        priorityCombo.setCaption(messageSource.getMessage("site.table.priority"));
        expirationDate.setCaption(messageSource.getMessage("site.table.expiration_date"));
        statusCheckBox.setCaption(messageSource.getMessage("site.status.question"));
        okBtn.setCaption(messageSource.getMessage("site.button.ok"));
        cancelBtn.setCaption(messageSource.getMessage("site.button.cancel"));

    }


}
