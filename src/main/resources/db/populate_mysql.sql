--
-- Clear the TODO_LIST table
--
TRUNCATE TABLE TODO_LIST;

ALTER TABLE TODO_LIST AUTO_INCREMENT = 100000;

--
-- TODO_LIST dump
--
INSERT INTO TODO_LIST (name, description, priority, creation_date, expiration_date, status)
VALUES
  ('todo01', '', 1, '2014-10-20', '2014-10-26', 0),
  ('todo02', '', 5, '2014-10-20', '2014-10-26', 0),
  ('todo03', '', 4, '2014-10-20', '2014-11-05', 0),
  ('todo04', '', 3, '2014-10-20', '2014-11-05', 0);