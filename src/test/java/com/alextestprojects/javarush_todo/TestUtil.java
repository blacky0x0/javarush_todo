package com.alextestprojects.javarush_todo;

import com.alextestprojects.javarush_todo.model.Todo;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Author: blacky
 * Date: 26.10.14
 */
public class TestUtil {
    public static final Date EXPIRATION_DATE = getExpirationDate(2014, Calendar.OCTOBER, 26);
    public static final Date EXPIRATION_DATE2 = getExpirationDate(2014, Calendar.NOVEMBER, 5);
    public static final int TODO01_ID = Todo.START_SEQ;
    public static final int TODO02_ID = Todo.START_SEQ + 1;
    public static final int TODO03_ID = Todo.START_SEQ + 2;
    public static final int TODO04_ID = Todo.START_SEQ + 3;
    public static final int TODO05_ID = Todo.START_SEQ + 4;
    public static final Todo TODO01 = new Todo("todo01", "", 1, EXPIRATION_DATE, false);
    public static final Todo TODO02 = new Todo("todo02", "", 5, EXPIRATION_DATE, false);
    public static final Todo TODO03 = new Todo("todo03", "", 4, EXPIRATION_DATE2, false);
    public static final Todo TODO04 = new Todo("todo04", "", 3, EXPIRATION_DATE2, false);
    public static final Todo TODO05 = new Todo("todo05", "", 2, EXPIRATION_DATE2, true);
    public static final String UPDATED_TODO = "UpdatedTodo";

    static {
        TODO01.setId(TODO01_ID);
        TODO02.setId(TODO02_ID);
        TODO03.setId(TODO03_ID);
        TODO04.setId(TODO04_ID);
        // javax.persistence.PersistenceException:
        // org.hibernate.PersistentObjectException:
        // detached entity passed to persist
        //TODO05.setId(TODO05_ID);
    }

    public static Date getExpirationDate(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        return c.getTime();
    }

    public static void assertToStringEquals(Object o1, Object o2) {
        assertEquals(toString(o1), toString(o2));
    }

    public static String toString(Object o1) {
        return o1 == null ? null : o1.toString();
    }
}
