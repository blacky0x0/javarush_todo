package com.alextestprojects.javarush_todo.service;

import com.alextestprojects.javarush_todo.model.Todo;
import com.alextestprojects.javarush_todo.util.DbPopulator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;

import static com.alextestprojects.javarush_todo.TestUtil.*;

/**
 * Author: blacky
 * Date: 25.10.14
 */
@ContextConfiguration({
        "classpath:spring/spring-app.xml",
        "classpath:spring/spring-db.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class TodoServiceTest {

    @Autowired
    private DbPopulator dbPopulator;

    @Autowired
    private TodoService todoService;

    @Before
    public void setUp() throws Exception {
        // set up data before test
        dbPopulator.execute();
    }

    @After
    public void tearDown() {
        // restore data after test
        dbPopulator.execute();
    }


    @Test
    public void testGet() throws Exception {
        assertToStringEquals(todoService.get(TODO01_ID), TODO01);
    }

    @Test
    public void testGetAll() throws Exception {
        assertToStringEquals(Arrays.asList(TODO01, TODO04, TODO03, TODO02), todoService.getAll());
    }

    @Test
    public void testSave() throws Exception {
        todoService.save(TODO05);
        assertToStringEquals(todoService.get(TODO05_ID), TODO05);
    }

    @Test
    public void testUpdate() throws Exception {
        Todo todo = todoService.get(TODO01_ID);
        todo.setName(UPDATED_TODO);
        todoService.update(todo);
        assertToStringEquals(todoService.get(TODO01_ID).getName(), UPDATED_TODO);
    }

    @Test
    public void testDelete() throws Exception {
        todoService.delete(TODO02_ID);
        assertToStringEquals(Arrays.asList(TODO01, TODO04, TODO03), todoService.getAll());
    }
}
